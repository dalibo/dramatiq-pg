__libpq_version__: int


class InterfaceError(Exception):
    ...


class OperationalError(Exception):
    ...
